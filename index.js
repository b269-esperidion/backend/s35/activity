const express = require("express");

// Mongoose is a package that allows creating of schemas to our model our data structures
// also has access to a number of methods for manipulating our database
const mongoose = require("mongoose");

const app = express();
const port= 3001;

// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://Ronielmanzano:admin123@zuitt.xquntz3.mongodb.net/s35?retryWrites=true&w=majority",
	{	
		// allows us to avoid any current and future errors while connecting to MongoDB
		useNewUrlParser: true,
		useUnifiedTopology: true
	}

	);
// Allows to handle errors when the initial connection is established 
let db= mongoose.connection;

// console.error.bind(console) allows us to print errors in the browser console and in the terminal
// "connection error" is the message that will display if an error is encountered
db.on("error", console.error.bind(console, "connection error"));
// If the connection is successful, out in the console
db.once("open", () => console.log("We're connected to the cloud database"));
// Connecting to MongoDB Atlas End

app.use(express.json());
app.use(express.urlencoded({extended: true}));

//[SECTION] Mongoose Schemas

const taskSchema = new mongoose.Schema({
	username: String,
	password: String,
	
})

// [SECTION] Mongoose Models

// Models must be in singular form and capitalized
const Task = mongoose.model("Task", taskSchema);


app.post("/signup", (req,res) => {
		let newTask = new Task ({
				username: req.body.username,
				password: req.body.password
			});
			newTask.save().then((savedTask, saveerr) => {
				if(saveerr){
					return console.error(saveerr);
				} else {
					return res.send("New user registered!");
				
				}
			})
		
		
	});

app.listen(port, () => console.log(`Service running at port ${port}`));

